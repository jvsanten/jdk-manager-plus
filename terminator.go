package main

import "os"

func exit(exitcode int64, message string) string {
	println(message)
	os.Exit(int(exitcode))
	return ""
}
