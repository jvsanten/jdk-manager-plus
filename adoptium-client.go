package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"runtime"
)

func findVersionPackage(versionNumber string) JdkBinaryPackage {
	resp, err := http.Get("https://api.adoptium.net/v3/assets/latest/" + versionNumber + "/hotspot?vendor=eclipse")
	if err == nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			exit(1, "Couldn't process data for version "+versionNumber)
		}
		var jdk []Jdk
		json.Unmarshal([]byte(responseData), &jdk)

		currentOs := runtime.GOOS
		arch := runtime.GOARCH

		var targetJdk Jdk

		for i := 0; i < len(jdk); i++ {
			if (jdk[i].Binary.Os == currentOs || jdk[i].Binary.Os == "mac" && currentOs == "darwin") && architectureMatch(jdk[i].Binary.Architecture, arch) && jdk[i].Binary.ImageType == "jdk" {
				targetJdk = jdk[i]
			}
		}

		if targetJdk == (Jdk{}) {
			exit(1, "Couldn't get find JDK version "+versionNumber+" for your plaform")
		} else {
			return (targetJdk.Binary.Package)
		}
	} else {
		exit(1, "Couldn't process data for version "+versionNumber)
	}
	return JdkBinaryPackage{}
}

type JdkRelease struct {
	VersionNumber int64
	LtsRelease    bool
}

func getAvailableReleases() []JdkRelease {
	var availableReleases []JdkRelease
	resp, err := http.Get("https://api.adoptium.net/v3/info/available_releases")
	if err == nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			var releases AvailableReleases
			json.Unmarshal([]byte(responseData), &releases)
			for i := 0; i < len(releases.AvailableReleases); i++ {
				availableReleases = append(availableReleases, JdkRelease{releases.AvailableReleases[i], sliceContains(releases.AvailableLtsReleases, releases.AvailableReleases[i])})
			}
			return availableReleases
		}
	}
	exit(1, "Could not get available versions")
	return availableReleases
}

func sliceContains(slice []int64, value int64) bool {
	for i := 0; i < len(slice); i++ {
		if slice[i] == value {
			return true
		}
	}
	return false
}

func architectureMatch(foundArch string, targetArch string) bool {
	return (foundArch == "aarch64" && targetArch == "arm64") ||
		(foundArch == "x64" && targetArch == "amd64") ||
		(foundArch == "x32" && targetArch == "386")

}
