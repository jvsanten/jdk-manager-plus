module jvsanten/jdk-manager-plus

go 1.18

require (
	github.com/otiai10/copy v1.7.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
