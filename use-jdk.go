package main

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
)

func useJdk(version string, jdkmConfig JdkmConfig) {
	versionDir := filepath.Join(jdkmConfig.InstallDir, jdkmConfig.JdkDirPrefix+version)
	activeDir := filepath.Join(jdkmConfig.InstallDir, "ActiveJDK")
	if fileInfo, err := os.Stat(versionDir); err == nil && fileInfo.IsDir() {
		if _, err := os.Lstat(activeDir); err != nil {
			if !errors.Is(err, os.ErrNotExist) {
				exit(1, activeDir+"is present but not a symlink")
			}
		} else {
			os.Remove(activeDir)
		}
		if runtime.GOOS != "windows" {
			if err := os.Symlink(versionDir, activeDir); err == nil {
				outputSuccess(versionDir, activeDir)
			} else {
				println(err.Error())
			}
		} else {
			cmd := exec.Command("cmd", "/c", "mklink", "/J", activeDir, versionDir)
			if err := cmd.Run(); err == nil {
				outputSuccess(versionDir, activeDir)
			} else {
				println("Symlink could not be created: ")
				println(err.Error())
				os.Exit(1)
			}
		}
	} else {
		exit(1, "Target directory not found: "+versionDir)
	}
}

func outputSuccess(versionDir string, activeDir string) {
	println("Created link: " + activeDir + "\nTargetting: " + versionDir)
	println("Please make sure your JAVA_HOME is set to: " + activeDir)
}
