package main

type Jdk struct {
	Binary      JdkBinary  `json:"binary"`
	ReleaseName string     `json:"release_name"`
	Vendor      string     `json:"vendor"`
	Version     JdkVersion `json:"version"`
}

type JdkBinary struct {
	Architecture  string           `json:"architecture"`
	DownloadCount int64            `json:"download_count"`
	HeapSize      string           `json:"heap_size"`
	ImageType     string           `json:"image_type"`
	JvmImpl       string           `json:"jvm_impl"`
	Os            string           `json:"os"`
	Package       JdkBinaryPackage `json:"package"`
	Project       string           `json:"project"`
	ScmRef        string           `json:"scm_ref"`
	UpdatedAt     string           `json:"updated_at"`
}

type JdkBinaryPackage struct {
	Checksum      string `json:"checksum"`
	ChecksumLink  string `json:"checksum_link"`
	DownloadCount int64  `json:"download_count"`
	Link          string `json:"link"`
	MetadataLink  string `json:"metadata_link"`
	Name          string `json:"name"`
	Size          int64  `json:"size"`
}

type JdkVersion struct {
	Build          int64  `json:"build"`
	Major          int64  `json:"major"`
	Minor          int64  `json:"minor"`
	OpenjdkVersion string `json:"openjdk_version"`
	Security       int64  `json:"security"`
	Semver         string `json:"semver"`
}

type AvailableReleases struct {
	AvailableLtsReleases     []int64 `json:"available_lts_releases"`
	AvailableReleases        []int64 `json:"available_releases"`
	MostRecentFeatureRelease int64   `json:"most_recent_feature_release"`
	MostRecentFeatureVersion int64   `json:"most_recent_feature_version"`
	MostRecentLts            int64   `json:"most_recent_lts"`
	TipVersion               int64   `json:"tip_version"`
}
