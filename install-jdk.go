package main

import (
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/otiai10/copy"
)

func installJdk(version string, jdkmConfig JdkmConfig) {
	if version == "all" {
		installAll(false, jdkmConfig)
	} else if version == "all-lts" {
		installAll(true, jdkmConfig)
	} else {
		installVersion(version, jdkmConfig)
	}

}

func installVersion(version string, jdkmConfig JdkmConfig) {
	var binaryPackage JdkBinaryPackage = findVersionPackage(version)
	fileLoc := downloadFile(binaryPackage.Link, jdkmConfig)
	unpackedDir := ""
	verifyFile(fileLoc, binaryPackage.Checksum)
	if strings.HasSuffix(fileLoc, ".tar.gz") {
		tarPath := unGzip(fileLoc)
		unpackedDir = unTar(tarPath, jdkmConfig.TempDir)
	} else if strings.HasSuffix(fileLoc, ".zip") {
		unpackedDir = unzipFile(fileLoc, jdkmConfig.TempDir)
	} else {
		exit(1, "Cannot unpack unknown archive type: "+fileLoc)
	}
	destDir := filepath.Join(jdkmConfig.InstallDir, jdkmConfig.JdkDirPrefix+version)
	moveIntoPlace(unpackedDir, destDir, version)
	processCerts(jdkmConfig.AdditionalCertFiles, destDir, version == "8")
}

func installAll(ltsOnly bool, jdkmConfig JdkmConfig) {
	for _, release := range getAvailableReleases() {
		if !ltsOnly || release.LtsRelease {
			installVersion(strconv.FormatInt(int64(release.VersionNumber), 10), jdkmConfig)
		}
	}
}

func downloadFile(link string, jdkmConfig JdkmConfig) string {
	println("Downloading: " + link)
	os.MkdirAll(jdkmConfig.TempDir, os.ModePerm)
	if resp, err := http.Get(link); err == nil {
		defer resp.Body.Close()
		fileName := link[strings.LastIndex(link, "/")+1:]
		if out, err := os.Create(jdkmConfig.TempDir + "/" + fileName); err == nil {
			defer out.Close()
			if _, err := io.Copy(out, resp.Body); err == nil {
				println("Download Complete")
				return filepath.Join(jdkmConfig.TempDir, fileName)
			}
		}
	}
	return exit(1, "Download failed for: "+link)
}

func verifyFile(fileLoc string, checksum string) {
	hasher := sha256.New()
	if file, err := os.Open(fileLoc); err == nil {
		if _, err := io.Copy(hasher, file); err == nil {
			if hex.EncodeToString(hasher.Sum(nil)) == checksum {
				println("Download Verified")
				return
			} else {
				exit(1, "File: "+fileLoc+
					"\nExpected Hash: "+checksum+
					"\nActual Hash:   "+hex.EncodeToString(hasher.Sum(nil))+
					"\nFile hash doesn't meet expected hash")
			}
		}
	}
	exit(1, "Could not verify file")
}

func unGzip(gzPath string) string {
	target := gzPath[:strings.LastIndex(gzPath, ".gz")]
	if reader, err := os.Open(gzPath); err == nil {
		defer reader.Close()
		if archive, err := gzip.NewReader(reader); err == nil {
			defer archive.Close()
			if writer, err := os.Create(target); err == nil {
				defer writer.Close()
				if _, err := io.Copy(writer, archive); err == nil {
					if err := os.Remove(gzPath); err != nil {
						println("Could not remove gzfile: " + gzPath)
					}
					return target
				}
			}
		}
	}
	return exit(1, "Coulnd't unpack gz file")
}

func unTar(tarPath string, tempDir string) string {
	unpackedDir := ""
	if reader, err := os.Open(tarPath); err == nil {
		defer reader.Close()
		tarReader := tar.NewReader(reader)
		for {
			header, err := tarReader.Next()
			if err == io.EOF {
				println("Download Unpacked")
				if err := os.Remove(tarPath); err != nil {
					println("Could not remove tarfile: " + tarPath)
				}
				return unpackedDir
			} else if err != nil {
				break
			}
			path := filepath.Join(tempDir, header.Name)
			info := header.FileInfo()
			if info.IsDir() {
				if unpackedDir == "" {
					unpackedDir = filepath.Join(tempDir, info.Name())
				}
				if err = os.MkdirAll(path, info.Mode()); err != nil {
					break
				}
			} else {
				fileMode := getFileMode(info.Mode())

				if file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, fileMode); err == nil {
					defer file.Close()
					if _, err := io.Copy(file, tarReader); err != nil {
						break
					}
				}
			}
		}
	}
	return exit(1, "Couldn't unpack tarball")
}

func unzipFile(zipFileLoc string, tempDir string) string {
	unpackedDir := ""
	if archive, err := zip.OpenReader(zipFileLoc); err == nil {
		defer archive.Close()
		for _, file := range archive.File {
			targetPath := filepath.Join(tempDir, file.Name)
			if !strings.HasPrefix(targetPath, filepath.Clean(tempDir)+string(os.PathSeparator)) {
				println("invalid file path")
				return exit(1, "Couldn't unpack zip")
			}
			if file.FileInfo().IsDir() {
				if unpackedDir == "" {
					unpackedDir = filepath.Join(tempDir, file.Name)
				}
				os.MkdirAll(targetPath, file.FileInfo().Mode())
			} else {
				// don't do read only files (for future management)
				fileMode := getFileMode(file.Mode())
				if fileOut, err := os.OpenFile(targetPath, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, fileMode); err == nil {
					if archiveFile, err := file.Open(); err == nil {
						if _, err := io.Copy(fileOut, archiveFile); err == nil {
							fileOut.Close()
							archiveFile.Close()
						} else {
							return exit(1, "Couldn't unpack zip")
						}
					} else {
						return exit(1, "Couldn't unpack zip")
					}
				} else {
					return exit(1, "Couldn't unpack zip")
				}
			}
		}
		archive.Close()
	} else {
		return exit(1, "Couldn't unpack zip")
	}
	if err := os.Remove(zipFileLoc); err != nil {
		println("Could not remove zip file: " + zipFileLoc)
		println(err.Error())
	}
	println("Download Unpacked")
	return unpackedDir
}

func getFileMode(fmode os.FileMode) os.FileMode {
	if fmode.String()[2] == "-"[0] {
		return os.FileMode(0666)
	} else {
		return fmode
	}
}

func moveIntoPlace(source string, destination string, version string) {
	if err := copy.Copy(source, destination, copy.Options{OnDirExists: func(src, dest string) copy.DirExistsAction { return copy.Replace }}); err != nil {
		println(err.Error())
		exit(1, "Could not move new JDK to: "+destination)
	}
	if err := os.RemoveAll(source); err != nil {
		println("WARN - Could not remove temporary files after move")
	}
	println("New JDK installed in: " + destination)
	println("Run 'jdkm use " + version + "' to use this JDK version")
}

func processCerts(certs []string, jdkDir string, legacy bool) {

	keytool := filepath.Join(jdkDir, "bin")
	keytool = filepath.Join(keytool, "keytool")
	keystore := jdkDir
	if legacy {
		keystore = filepath.Join(keystore, "jre")
	}
	keystore = filepath.Join(keystore, "lib")
	keystore = filepath.Join(keystore, "security")
	keystore = filepath.Join(keystore, "cacerts")

	if len(certs) > 0 {
		if err := copy.Copy(keystore, keystore+".backup"); err != nil {
			println("WARNING: cacerts backup could not be created before importing certs")
		}
	}

	for _, cert := range certs {
		if fileinfo, err := os.Stat(cert); err == nil {
			cmd := exec.Command(keytool, "-importcert", "-trustcacerts", "-noprompt", "-file", cert, "-keystore", keystore, "-alias", "\""+fileinfo.Name()+"\"", "-storepass", "changeit")
			if err := cmd.Run(); err == nil {
				println("Added certificate: " + cert + " To certsfile: " + keystore)
			} else {
				exit(1, "Could not add cert: "+cert+" To certsfile: "+keystore)
			}
		} else {
			exit(1, "Cert file not found: "+cert)
		}
	}
}
