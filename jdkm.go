package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"gopkg.in/yaml.v3"
)

func main() {
	jdkmConfig := readConfig()
	cleanArgs := []string{"", "", "", "", "", "", "", "", "", ""}
	for i, arg := range os.Args {
		if i > 0 {
			cleanArgs[i-1] = strings.ToLower(arg)
		}
	}
	if isHelpString(cleanArgs[0]) {
		help()
	} else if cleanArgs[0] == "install" {
		if cleanArgs[1] == "" {
			println("Please specify version(s) to install")
		} else {
			installJdk(cleanArgs[1], jdkmConfig)
		}
	} else if cleanArgs[0] == "list" {
		listJdks(cleanArgs[1])
	} else if cleanArgs[0] == "use" {
		useJdk(cleanArgs[1], jdkmConfig)
	} else if cleanArgs[0] == "config" {
		config(jdkmConfig)
	} else {
		println("Action not recognised or no action found: ")
		help()
	}
}

func help() {
	// println("TODO: full help text")
	println()
	println("Actions: \n\tinstall <version|all-lts|all>\t install one or more versions, no default" +
		"\n\tlist <all|lts>\t\t\t list all available (lts) versions, defaults to list all" +
		"\n\tuse <version>\t\t\t use a specific version as the active jdk, no default" +
		"\n\tconfig \t\t\t\t shows the current config and information on the config file")
}

func config(jdkmConfig JdkmConfig) {
	println("Current config in yaml format:\n")
	println("----------------------------------\n")
	if yamlBytes, err := yaml.Marshal(jdkmConfig); err == nil {
		println(string(yamlBytes))
	}
	println("----------------------------------\n")
	if dirname, err := os.UserHomeDir(); err == nil {
		println("To override any setting put a yaml in this format in this location:\n" + filepath.Join(dirname, ".jdkm.yaml"))
		println("You only need to include the values you want to override")
		println("additionalCerts takes an array of fully qualified paths. For example:\n" + filepath.Join(jdkmConfig.InstallDir, "my-cert.pem"))
	}

}

func isHelpString(arg string) bool {
	return arg == "help" || arg == "--help" || arg == "-h"
}

func readConfig() JdkmConfig {
	jdkmConfig := defaultConfig()
	if dirname, err := os.UserHomeDir(); err == nil {
		if _, err := os.Stat(filepath.Join(dirname, ".jdkm.yaml")); err == nil {
			// path/to/whatever exists
			if yamlFile, err := ioutil.ReadFile(filepath.Join(dirname, ".jdkm.yaml")); err == nil {
				if err = yaml.Unmarshal(yamlFile, &jdkmConfig); err == nil {
					return jdkmConfig
				}
			}
		}
	} else {
		println("Could not read config file - using defaults")
	}
	return jdkmConfig
}

func defaultConfig() JdkmConfig {
	if dirname, err := os.UserHomeDir(); err == nil {
		currentOs := runtime.GOOS
		if currentOs == "linux" {
			return JdkmConfig{TempDir: "/tmp", InstallDir: dirname + "/.jdkm", JdkDirPrefix: "OpenJDK"}
		} else if currentOs == "windows" {
			return JdkmConfig{TempDir: "C:\\TEMP", InstallDir: dirname + "\\jdkm", JdkDirPrefix: "OpenJDK"}
		} else if currentOs == "darwin" {
			return JdkmConfig{TempDir: "/tmp", InstallDir: dirname + "/.jdkm", JdkDirPrefix: "OpenJDK"}
		} else {
			exit(1, "Unknown OS not supported")
		}
	} else {
		exit(1, "User home directory not found")
	}
	return JdkmConfig{}
}

type JdkmConfig struct {
	TempDir             string   `yaml:"tempDir"`
	InstallDir          string   `yaml:"installDir"`
	AdditionalCertFiles []string `yaml:"additionalCerts"`
	JdkDirPrefix        string   `yaml:"jdkDirPrefix"`
}
