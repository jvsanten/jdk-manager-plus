package main

import "fmt"

func listJdks(arg string) {
	if arg == "lts" {
		listLtsJdks()
	} else {
		listAllJdks()
	}
}

func listLtsJdks() {
	jdkReleases := getAvailableReleases()
	ltsReleases := []JdkRelease{}
	for _, jdkRelease := range jdkReleases {
		if jdkRelease.LtsRelease {
			ltsReleases = append(ltsReleases, jdkRelease)
		}
	}
	printReleases(ltsReleases)
}

func listAllJdks() {
	jdkReleases := getAvailableReleases()
	printReleases(jdkReleases)
}

func printReleases(jdkReleases []JdkRelease) {
	println("Currently available JDK versions:")
	println("-------------------------")
	println("| Version |\tLTS\t|")
	println("|---------|-------------|")
	for _, jdkRelease := range jdkReleases {
		print("|   ")
		jdkString := fmt.Sprint(jdkRelease.VersionNumber)
		if jdkRelease.LtsRelease {
			jdkString += "\t  |\t x\t|"
		} else {
			jdkString += "\t  |\t  \t|"
		}
		println(jdkString)
	}
	println("-------------------------")
}
