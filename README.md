# JDK-Manager-Plus

This project started as (or at time of writing is starting as) a project with two goals

1) Give me an excuse to play around with GO 
2) Deal with the annoying issues of managing JDKs especially at my job

## So what's the problem you're trying to solve?

Well at home I run Linux (Arch BTW). Which does pretty well with installing and managing different JDKs. The ```archlinux-java``` command makes it pretty easy to switch between different JDKs I may have installed - which is handy when dealing with legacy projects that won't run in anything newer than Java 8, but also with newer projects that won't run in anything older than Java 11. (And indeed for quickly checking Java 17 compatibility now that we have a new LTS release to play with after 11.)

At work, on windows, there's no such luck. I've made some scripts to make things easier for myself, but what I'd ultimately want is a quick way to download and switch between various JDK versions. Basically something like NVM but for JDKs.

### Aren't we reinveting the wheel here? (Don't JDK managers already exist?)

They do, but right now none of the current options quite seem to do what I'd like them to do. Various 'situation specific' features, such as adding custom certs to any JDK installed (to allow https interaction with internal repos), don't appear to be covered. ~~Which gives me a perfect excuse to~~ *Leaves me no alternative but to* write my own...

## State of the Project

Let's start with a disclaimer: this is (probably) terrible code. It's still very much my first go at writing go *insert punny chuckle here*

Currently the project can get a list of available JDK versions from the adoptium.net api, download the version(s) you specify, set up an 'ActiveJDK' symlink folder pointing to one of the installed JDKs, so you can use that to quickly switch JDKs when desired. When given no arguments the program prints a help text:

```
Actions: 
        install <version|all-lts|all>    install one or more versions, no default
        list <all|lts>                   list all available (lts) versions, defaults to list all
        use <version>                    use a specific version as the active jdk, no default
        config                           shows the current config and information on the config file
```

Use the config command to show the current config (default if you've not specified any yourself) and also get information on the config file. The config file is also allows you to specify one or more certificates that should be added to the cacerts of any newly installed JDK.

At present the install command always downloads and installs the JDK(s) specified, it's a very stupid 'install and upgrade in one', which doesn't bother to check whether the newly downloaded version is any newer than the one you have. It also doesn't bother to check whether 'available' JDKs are available for your platform (though that shouldn't crop up too much except right as a new version is released).

## License

This code is available under MIT licence (as per the LICENSE file).